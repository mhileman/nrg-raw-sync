import os
import sys
import yaml
import socket
import requests
#from optparse import OptionParser
from dateutil.parser import parse
from datetime import datetime, timedelta

from hcpxnat.interface import HcpInterface

'''
Deletes XNAT sessions and cplxraw older than specified time from the relay
Deletion intervals set in config.yaml
'''

with open("/raw/scripts/config.yaml") as f:
    cfg = yaml.load(f)

# Make sure we use newest log since ozmt appends year-month
log_file = cfg['sync']['logfile'] + "." + datetime.now().strftime("%Y-%m")

# Module that talks to XNAT instances
relay_xnat = HcpInterface(
    config=os.path.join(os.path.expanduser('~'), ".hcpxnat_relay.cfg"))
intradb = HcpInterface(
    config=os.path.join(os.path.expanduser('~'), ".hcpxnat_intradb.cfg"))


def main():
    cleanup_xnat()
    cleanup_raw()


def cleanup_xnat():
    # Return if there's no local XNAT cleanup section configured
    try:
        interval_days = cfg['xnat']['local_cleanup_interval_days']
    except KeyError as e:
        with open(log_file, 'a') as log:
            log.write("No XNAT cleanup interval found in config.yaml\n")
            log.write(e.message)
        return

    for project in cfg['xnat']['projects']:
        relay_xnat.project = project
        intradb.project = project
        cleanup_project(interval_days)


def cleanup_project(interval_days):
    with open(log_file, 'a') as log:
        log.write("Deleting sessions older than {} days in project {} on {}\n".format(
            interval_days, intradb.project, relay_xnat.url))

    relay_sessions = get_sessions(relay_xnat)
    intradb_sessions = get_sessions(intradb)
    intradb_session_labels = []

    # Return if we get an empty list for either XNAT instance
    if not relay_sessions or not intradb_sessions:
        return

    incomplete_session_labels = get_incomplete_sessions(relay_sessions)
    # print incomplete_session_labels

    for session in intradb_sessions:
        intradb_session_labels.append(session['label'])

    for session in relay_sessions:
        relay_session_date = parse(session['insert_date'])
        relay_project = relay_xnat.project

        # Evaluates to True or False
        old_enough = datetime.now() - relay_session_date > \
            timedelta(days=int(interval_days))

        # If the relay session is old enough and is on Intradb and is complete
        if old_enough and \
            session['label'] in intradb_session_labels and \
            session['label'] not in incomplete_session_labels:

            with open(log_file, 'a') as log:
                log.write("{s} older than {d} days.\n".format(
                    s=session['label'], d=interval_days))
                log.write("DELETING {s} from {p} on {u}\n".format(
                    s=session['label'], p=relay_project, u=relay_xnat.url))

            del_uri = '/data/projects/{p}/experiments/{e}?removeFiles=true'.format(
                p=relay_project, e=session['label'])
            relay_xnat.delete(del_uri)
            #print del_uri

    # Delete all subjects that have no experiment data
    with open(log_file, 'a') as log:
        log.write("Deleting subjects that have no experiments.\n")

    subjects = relay_xnat.getSubjects(relay_xnat.project)

    for subject in subjects:
        uri = '/data/projects/{p}/subjects/{s}/experiments'.format(
            p=relay_xnat.project, s=subject['ID'])
        experiments = relay_xnat.getJson(uri)

        # If there are no experiments
        if not len(experiments):
            with open(log_file, 'a') as log:
                log.write("Deleting empty subject " + subject['label'])
            del_uri = '/data/projects/{p}/subjects/{s}'.format(
                p=relay_xnat.project, s=subject['ID'])
            relay_xnat.delete(del_uri)
            #print del_uri


def get_sessions(xnat):
    sessions = []
    try:
        sessions = xnat.getSessions(project=xnat.project)
    except requests.exceptions.HTTPError as e:
        with open(log_file, 'a') as log:
            log.write("Couldn't get sessions for project {} on {}\n".format(
                xnat.project, xnat.url))
            log.write(e.message + '\n')

    if not sessions:
        with open(log_file, 'a') as log:
            log.write("No sessions found for project {} on {}\n".format(
                xnat.project, xnat.url))

    return sessions


def get_incomplete_sessions(relay_sessions):
    # Check that all sessions on relay have same number scans as Intradb

    incomplete_session_labels = []

    for relay_session in relay_sessions:
        # print "Checking {} for completeness".format(relay_session['label'])
        with open(log_file, 'a') as log:
            log.write("INFO: Checking that {} has all scans\n".format(
                relay_session['label']))

        relay_xnat.session_label = relay_session['label']
        relay_xnat.subject_label = relay_xnat.getSessionSubject()
        # Assuming the subject and session labels match at both ends
        relay_scan_uri = "/data/projects/{}/subjects/{}/experiments/{}/scans".format(
            relay_xnat.project, relay_xnat.subject_label, relay_session['label'])
        intradb_scan_uri = "/data/projects/{}/subjects/{}/experiments/{}/scans".format(
            intradb.project, relay_xnat.subject_label, relay_session['label'])

        try:
            relay_scan_count = len(relay_xnat.getJson(relay_scan_uri))
            intradb_scan_count = len(intradb.getJson(intradb_scan_uri))
        except:
            with open(log_file, 'a') as log:
                # print "Missing " + relay_session['label']
                log.write("WARNING: {} is missing from Intradb or the relay\n".format(
                    relay_session['label']))
                continue
        
        if relay_scan_count != intradb_scan_count:
            with open(log_file, 'a') as log:
                log.write("ERROR: Scan count mismatch for session\n".format(
                    interval_days, intradb.project, relay_xnat.url))
            incomplete_session_labels.append(relay_xnat.session_label)
            # TODO send an email to relay admin, use relay email server

    return incomplete_session_labels


def cleanup_raw():
    # find /raw/data/staging/ -mtime +N -exec rm -f {} \;
    # Return if there's no local raw cleanup section configured
    with open(log_file, 'a') as log:
        log.write("Attempting to delete raw older than {} days\n".format(
            cfg['relay']['local_cleanup_interval_days']))

    try:
        interval_days = cfg['relay']['local_cleanup_interval_days']
    except KeyError as e:
        with open(log_file, 'a') as log:
            log.write("No Relay cleanup interval found in config.yaml\n")
            log.write(e.message)
        return

    # Get .cplxraw files in the staging directory
    raw_staging = os.listdir(cfg['relay']['source'])

    # Get .cplxraw file listing from backup server
    relay_hostname = socket.gethostname().split('.')[0]
    host = "https://hcp-ops.humanconnectome.org"
    uri = "/api/relay/hosts/{h}/nodes/backup/ls".format(h=relay_hostname)

    r = requests.get(host + uri)
    backup_listing = r.text.split('\n')[:-1]
    backup_files = []

    # Check each line in the ls -lht and build file list
    for f in backup_listing[1:]:
        substrings = f.split(' ')
        fname = substrings[-1]
        backup_files.append(fname)

    with open(log_file, 'a') as log:
        log.write("Files on relay staging {}".format(backup_files))

    for f in raw_staging:
        fpath = os.path.join(cfg['relay']['source'], f)
        mtime = os.path.getmtime(fpath)
        file_date = datetime.fromtimestamp(mtime)
        now = datetime.now()
        old_enough = now - file_date > timedelta(days=int(interval_days))

        if f in backup_files and old_enough:
            with open(log_file, 'a') as log:
                log.write("{f} older than {d} days. DELETING\n".format(
                    f=f, d=interval_days))
            os.remove(fpath)


if __name__ == "__main__":
    main()
