import os
import sys
import json
import yaml
import requests
import ConfigParser
from datetime import datetime

# from hcpxnat.interface import HcpInterface

cfg_file = os.path.join(os.path.expanduser('~'), ".hcpxnat_intradb.cfg")
intradb_cfg = ConfigParser.ConfigParser()
intradb_cfg.read(cfg_file)
remote_user = intradb_cfg.get('auth', 'username')
remote_pass = intradb_cfg.get('auth', 'password')
remote_host = intradb_cfg.get('site', 'hostname')

cfg_file = os.path.join(os.path.expanduser('~'), ".hcpxnat_relay.cfg")
relay_cfg = ConfigParser.ConfigParser()
relay_cfg.read(cfg_file)
local_user = relay_cfg.get('auth', 'username')
local_pass = relay_cfg.get('auth', 'password')
local_host = relay_cfg.get('site', 'hostname')

with open("/raw/scripts/config.yaml") as f:
    scripts_cfg = yaml.load(f)

log_file = scripts_cfg['sync']['logfile'] + "." + \
    datetime.now().strftime("%Y-%m")

# relay_xnat = HcpInterface(
#     config=os.path.join(os.path.expanduser('~'), ".hcpxnat_relay.cfg"))


for p in scripts_cfg['xnat']['projects']:
    with open(log_file, 'a') as log:
        log.write("Refreshing remote credentials token for " + p)

    # Get a token from the remote host
    print "Setting up credentials for project " + p
    print "Getting token from remote host"
    json_params = {
        "method": "GET",
        "url": remote_host + "/data/services/tokens/issue",
        "username": remote_user,
        "password": remote_pass
    }

    auth = (local_user, local_pass)
    uri = local_host + '/xapi/xsync/remoteREST'
    r = requests.post(uri, json=json_params, auth=auth)

    if r.status_code != 200:
        with open(log_file, 'a') as log:
            log.write("Unable to request remote token.\n")
            log.write("Request returned status " + str(r.status_code) + '\n')
        sys.exit(1)

    token_response = r.json()


    # Save the Xsync credentials on the local host
    print "Saving Xsync remote credentials"
    json_params = {
      "host": remote_host,
      "localProject": p,
      "remoteProject": p,
      "syncNewOnly": 'true',
      "alias": token_response['alias'],
      "secret": token_response['secret'],
      "username": token_response['xdatUserId'],
      "estimatedExpirationTime": token_response['estimatedExpirationTime']
    }
    data = json.dumps(json_params)

    uri = local_host + '/xapi/xsync/credentials/save/projects/{}'.format(p)
    headers = {'Content-Type': 'text/plain', 'Accept': 'application/json'}
    r = requests.post(uri, headers=headers, data=data, auth=auth)

    if r.status_code != 200:
        with open(log_file, 'a') as log:
            log.write(r.text + '\n')
            log.write("Unable to save remote credentials.")
            log.write("Request returned status " + str(r.status_code) + '\n')
        sys.exit(1)

    print r
